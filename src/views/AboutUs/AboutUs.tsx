import BreadCrumb from 'components/BreadCrumbs/BreadCrumbs'
import LetsTalk from 'components/LetsTalk/LetsTalkComp'
import Video from 'components/Video/Video'
import React from 'react'

function AboutUs() {
  const bgImage = {
    backgroundImage: "url('./static/images/background/bg-hero-about.png')",
  }

  const breadcrumbItems = [
    // { text: 'Home', link: '/' },
    { text: 'About Us' },
  ]

  const achivementData = [
    {
      icon: './static/images/icons/users.svg',
      total: '100+',
      name: 'Clients',
    },
    {
      icon: './static/images/icons/file-text.svg',
      total: '200+',
      name: 'Projects',
    },
    {
      icon: './static/images/icons/package.svg',
      total: '10',
      name: 'Product',
    },
  ]

  const images = [
    {
      url: './static/images/logo/logo-13.svg',
    },
    {
      url: './static/images/logo/logo-7.png',
    },
    {
      url: './static/images/logo/logo-10.png',
    },
    {
      url: './static/images/logo/logo-8.png',
    },
    {
      url: './static/images/logo/logo-9.png',
    },
    {
      url: './static/images/logo/logo-12.png',
    },
    {
      url: './static/images/logo/logo-11.png',
    },
    {
      url: './static/images/logo/logo-14.svg',
    },
  ]
  return (
    <div>
      {/* Hero */}
      <div
        className="h-[100vh] bg-cover relative bottom-[75px] after:block after:w-full after:h-[100vh] after:bg-gradient-to-r from-[#DF1E36] to-[rgba(172,24,42,0.85)] after:absolute after:bottom-0"
        style={bgImage}
      >
        <div className="z-[1] relative pt-[120px] mx-[80px] flex justify-between">
          <div>
            <BreadCrumb items={breadcrumbItems} />
            <h1 className="text-white font-bold text-[42px] leading-[63px] w-[497px]">
              “Great things in business are never done by one person; they're
              done by a team of people.”
            </h1>
            <img
              className="mt-7"
              src="./static/images/content/steve-job.png"
              alt=""
            />
            <p className="text-white font-semibold mt-4">
              Steve Jobs - Ex Apple Ceo
            </p>
          </div>
          <div className="z-[10] relative mt-[150px] -left-[200px]">
            <Video linkVideo="https://www.youtube.com/watch?v=SezDD2ULQ1o" />
            <h3 className="mt-[70px] font-medium text-[#fff] text-[24px] -ml-14">
              Watch Our Intro
            </h3>
          </div>
        </div>
          <div className="relative bottom-0 left-0  text-center items-center flex justify-center z-[10]">
            <img
              src="./static/images/icons/chevrons-down.svg"
              alt="arrow"
              className=" "
            />
          </div>
      </div>
      {/* end */}

      {/* About Section */}
      <section className="flex mx-[80px] justify-evenly px-20]  mb-[228px] mt-[144px]">
        <div className="w-[590px]">
          <h6 className="text-[#DF1E36] text-[20px] font-medium">About Us</h6>
          <h5 className="text-[#404258] font-bold text-[32px]">
            Nusantech at Glance
          </h5>
          <p className="font-medium text-[#404258] text-[20px] leading-[30px] font-[Exo]">
            Established since 2015, together we built this company with passion
            and persistence. PT Solusi Teknologi Nusantara as known as Nusantech
            is a company that focuses on information technology services and
            research.
          </p>
          <p className="font-medium text-[#404258] text-[20px] leading-[30px] font-[Exo]">
            Our Company tries to bring solution to technological problems in the
            Indonesian Archipelago (Nusantara). We committed to improve Human
            Resources in the IT field on a national scale and create IT
            solutions with global standards.
          </p>
        </div>
        <div className="relative text-center">
          <div className="absolute -top-3 left-1/2 transform translate-x-[130px] -z-10">
            <img src="./static/images/icons/titik.svg" alt="" />
          </div>
          <img
            className="block mx-auto relative z-0"
            src="./static/images/content/aboutcontent/content-1.png"
            alt=""
          />
          <div className="absolute bottom-10 left-[600px] w-[65px]">
            <img src="./static/images/icons/frame-polcadot.svg" alt="" />
          </div>
          <div className="absolute bottom-5 left-[84px] -z-10">
            <img src="./static/images/icons/titik.svg" alt="" />
          </div>
        </div>
      </section>
      {/* End */}

      {/* Visi & Misi Section */}
      <section className="flex justify-evenly mx-[80px]">
        <div className="relative text-center">
          <div className="absolute -top-3 left-1/2 transform translate-x-[130px] -z-10">
            <img src="./static/images/icons/titik.svg" alt="" />
          </div>
          <img
            className="block mx-auto relative z-0"
            src="./static/images/content/aboutcontent/content-1.png"
            alt=""
          />
          <div className="absolute bottom-10 left-[600px] w-[65px]">
            <img src="./static/images/icons/frame-polcadot.svg" alt="" />
          </div>
          <div className="absolute bottom-5 left-[84px] -z-10">
            <img src="./static/images/icons/titik.svg" alt="" />
          </div>
        </div>
        <div className="w-[600px] mt-6">
          <h5 className="text-[#404258] font-bold text-[36px]">Our Vision</h5>
          <p className="font-medium text-[#404258] text-[20px] font-[Exo] mb-7">
            The most trusted global IT service provider
          </p>
          <h5 className="text-[#404258] font-bold text-[36px]">Our Mision</h5>
          <p className="font-medium text-[#404258] text-[20px] leading-[30px] font-[Exo]">
            Deliver best quality and high performance IT team Build professional
            and expert team Create collaborative ecosystem and for innovation
          </p>
          <ul className="flex flex-col list-disc ms-6 leading-[43px]">
            <li className="text-[#FEA900] text-[30px] relative">
              <p className="font-medium text-[#404258] text-[20px] font-[Exo] absolute top-[5px]">
                {' '}
                Deliver best quality and high performance IT team
              </p>
            </li>
            <li className="text-[#FEA900] text-[30px] relative">
              <p className="font-medium text-[#404258] text-[20px] font-[Exo] absolute top-[5px]">
                {' '}
                Build professional and expert team
              </p>
            </li>
            <li className="text-[#FEA900] text-[30px] relative">
              <p className="font-medium text-[#404258] text-[20px] font-[Exo] absolute top-[5px]">
                Create collaborative ecosystem and for innovation
              </p>
            </li>
          </ul>
        </div>
      </section>
      {/* End */}

      {/* Section Achivement */}
      <section className="mx-[80px] flex justify-evenly mt-[130px] gap-x-[31px] items-center ">
        <div className="w-[301px]">
          <span className="text-[#DF1E36] font-[Exo] text-[20px] font-medium">
            Achievement
          </span>
          <h5 className="font-bold text-[#404258] text-[42px] mt-5">
            Helping our partners to achieve more
          </h5>
        </div>
        <div className="flex gap-x-6 relative">
          <div className="absolute transform -translate-x-[32px] -translate-y-[31px] -z-20">
            <img src="./static/images/icons/titik.svg" alt="" />
          </div>
          {achivementData.map((e, index) => (
            <div
              key={index}
              className="w-[300px] h-[380px] bg-[#fff] border rounded-[20px] text-center py-10"
            >
              <img
                className="bg-[#FFF0F2] p-5 rounded-full m-auto"
                src={e.icon}
                alt=""
              />
              <h3 className="text-[#DF1E36] font-bold text-[54px] mt-10">
                {e.total}
              </h3>
              <h5 className="text-[#404258] text-[28px] font-medium">
                {e.name}
              </h5>
            </div>
          ))}
        </div>
      </section>
      {/* end */}

      {/* Lets Talk Section */}
      <div className="mt-[128px] mb-20">
        <LetsTalk />
      </div>
      {/* end */}

      {/* Partner Section */}
      <section className="bg-[#F8F8F8]">
        <div className="flex justify-center gap-10 py-[48px] px-[88px]">
          {images.slice(0, 8).map((data) => (
            <img src={data.url} alt="" />
          ))}
        </div>
      </section>
      {/* End */}
    </div>
  )
}

export default AboutUs
