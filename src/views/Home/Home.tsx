import CardNews from 'components/Cards/CardNews'
import CardStudyCase from 'components/Cards/CardStudyCase'
import Video from 'components/Video/Video'
import { newsData } from 'data/newsData'

function Home() {
  const images = [
    {
      url: './static/images/logo/logo-1.png',
    },
    {
      url: './static/images/logo/logo-2.png',
    },
    {
      url: './static/images/logo/logo-3.png',
    },
    {
      url: './static/images/logo/logo-4.png',
    },
    {
      url: './static/images/logo/logo-5.png',
    },
    {
      url: './static/images/logo/logo-6.png',
    },
    {
      url: './static/images/logo/logo-7.png',
    },
    {
      url: './static/images/logo/logo-8.png',
    },
    {
      url: './static/images/logo/logo-9.png',
    },
    {
      url: './static/images/logo/logo-10.png',
    },
    {
      url: './static/images/logo/logo-11.png',
    },
    {
      url: './static/images/logo/logo-12.png',
    },
  ]

  const whatWeDoContent = [
    {
      title: 'Web Development',
      description:
        "Digitizing your business system into a Web Application that can increase the company's credibility and help connect with your partners and your consumers.",
    },
    {
      title: 'IT Professional Training Services',
      description:
        'We provide training services by IT Professionals that can help you expand HR skills and knowledge in your organization and company environment.',
    },
    {
      title: 'Mobile Applications',
      description:
        'Your business application will be more practical and can be accessed anywhere with a mobile-based application using the Android and iOS operating systems.',
    },
    {
      title: 'IT Engineer Team Services',
      description:
        'Get the best service in application development by our innovative and experienced young professional team, accept custom requests with the best methods, and can provide full support for your application.',
    },
  ]

  const studyCaseContent = [
    {
      image: './static/images/content/trimegah.png',
      nameCompany: 'Trimegah',
      title: 'Trimegah Equity Dashboard',
      webIcon: './static/images/icons/globe.svg',
    },
    {
      image: './static/images/content/pupr.png',
      nameCompany: 'Kementrian PUPR',
      title: 'Trimegah Equity Dashboard',
      webIcon: './static/images/icons/globe.svg',
      androidIocn: './static/images/icons/android.svg',
      iosIcon: './static/images/icons/apple.svg',
    },
    {
      image: './static/images/content/beyond.png',
      nameCompany: 'Beyond Run',
      title: 'Trimegah Equity Dashboard',
      webIcon: './static/images/icons/globe.svg',
      androidIocn: './static/images/icons/android.svg',
      iosIcon: './static/images/icons/apple.svg',
    },
    {
      image: './static/images/content/kemenkesehatan.png',
      nameCompany: 'Kementerian Kesehatan',
      title: 'Learning Management System',
      webIcon: './static/images/icons/globe.svg',
    },
    {
      image: './static/images/content/astra.png',
      nameCompany: 'Astra International',
      title: 'Astra Green Company',
      webIcon: './static/images/icons/globe.svg',
      androidIocn: './static/images/icons/android.svg',
      iosIcon: './static/images/icons/apple.svg',
    },
    {
      image: './static/images/content/shell.png',
      nameCompany: 'Shell Indonesia',
      title: 'Lubeanalyst Tracker',
      webIcon: './static/images/icons/globe.svg',
      androidIocn: './static/images/icons/android.svg',
      iosIcon: './static/images/icons/apple.svg',
    },
  ]
  return (
    <div>
      {/* Hero section */}
      <div className="mt-[116px] px-[110px] flex">
        <div className="w-[550px]">
          <h1 className="text-[#404258] font-bold text-[54px] leading-[81px]">
            PT. Solusi Teknologi Nusantara
          </h1>
          <h5 className="text-[#404258] font-medium text-[28px] -mt-5 leading-[42px]">
            Bring Greater Solutions to the Nations
          </h5>
          <div className="flex gap-6 mt-10">
            <button className="bg-[#DF1E36] w-[190px] h-[55px] flex items-center justify-center rounded-[8px] text-white font-semibold gap-2 shadow-[0_8px_16px_0px_rgba(231,22,42,0.04)]">
              Consult Now
              <img src="./static/images/icons/u_whatsapp.svg" alt="" />
            </button>
            <button className="border border-[#DF1E36] w-[150px] h-[55px] flex items-center justify-center rounded-[8px] font-semibold text-[#DF1E36]">
              Learn More
            </button>
          </div>
        </div>
        <div className="flex justify-center rounded-lg w-[580px] items-center ml-[340px]">
          <Video
            link="/static/images/content/content-home.png"
            linkVideo="https://www.youtube.com/watch?v=SezDD2ULQ1o"
            styleVideo="w-[760px] h-[600px] rounded-[14px] mb-[15px]"
          />
        </div>
      </div>
      {/* End */}

      {/* Partner Section */}
      <section className="bg-[#F8F8F8] pt-[46px] pb-[109px] px-[110px] mt-[175px]">
        <h5 className="text-center text-[#404258] text-[36px] font-bold">
          Partners who grow together with us
        </h5>
        <div className="flex justify-center mt-[61px]">
          {images.slice(0, 6).map((data) => (
            <img src={data.url} alt="" />
          ))}
        </div>
        <div className="flex justify-center gap-10 mt-10">
          {images.slice(6, 12).map((data) => (
            <img src={data.url} alt="" />
          ))}
        </div>
      </section>
      {/* End */}

      {/* What we do section */}
      <section className="bg-gradient-to-r from-[#DF1E36] to-[rgba(172,24,42,0.85)] p-[80px] text-white relative">
        <img
          className="absolute right-0 -top-5 z-[1]"
          src="./static/images/vector/map.svg"
          alt=""
        />
        <div className="flex gap-3">
          <h1 className="text-[42px] font-semibold mb-12">What we do</h1>
          <img
            className="-mt-12"
            src="./static/images/icons/yellow-dot.svg"
            alt=""
          />
        </div>
        <div className="flex flex-wrap gap-x-[90px] gap-y-[40px] w-[900px] relative z-10">
          {whatWeDoContent.map((e) => (
            <div className="w-[390px]">
              <h5 className="text-[24px] font-bold">{e.title}</h5>
              <p className="font-[400] leading-[27px] w-[366px]">
                {e.description}
              </p>
            </div>
          ))}
        </div>
      </section>
      {/* End */}

      {/* Sudy Case Section */}
      <section className="px-[80px] pt-[105px]">
        <div className="flex justify-between items-center">
          <div>
            <div className="flex gap-3">
              <h1 className="text-[42px] text-[#404258] font-semibold">
                Study Case
              </h1>
              <img
                className="-mt-5"
                src="./static/images/icons/yellow-dot.svg"
                alt=""
              />
            </div>
            <p className="w-[325px] text-[#404258] leading-[27px] font-medium ">
              We are glad to work with many clients who grow together with us.
            </p>
          </div>
          <button className="border border-[#DF1E36] w-[150px] h-[55px] flex items-center justify-center rounded-[8px] font-semibold text-[#DF1E36]">
            See More
            <img src="./static/images/icons/arrow-right.svg" alt="" />
          </button>
        </div>
        <div className="grid grid-cols-3 gap- mt-7">
          {studyCaseContent.map((e) => (
            <CardStudyCase
              image={e.image}
              nameCompany={e.nameCompany}
              title={e.title}
              webIcon={e.webIcon}
              androidIcon={e.androidIocn}
              iosIcon={e.iosIcon}
            />
          ))}
        </div>
        {/* <div className="flex gap-[24px] justify-center">
          {studyCaseContent.slice(3, 6).map((e) => (
            <CardStudyCase
              image={e.image}
              nameCompany={e.nameCompany}
              title={e.title}
              webIcon={e.webIcon}
              androidIcon={e.androidIocn}
              iosIcon={e.iosIcon}
            />
          ))}
        </div> */}
      </section>
      {/* End */}

      <div />

      {/* Lates New Section */}
      <section className="px-[80px] pt-[105px]">
        <div className="flex justify-between items-center">
          <div>
            <div className="flex gap-3">
              <h1 className="text-[42px] text-[#404258] font-semibold">
                Latest News
              </h1>
              <img
                className="-mt-5"
                src="./static/images/icons/yellow-dot.svg"
                alt=""
              />
            </div>
          </div>
          <button className="border border-[#DF1E36] w-[150px] h-[55px] flex items-center justify-center rounded-[8px] font-semibold text-[#DF1E36]">
            See More
            <img src="./static/images/icons/arrow-right.svg" alt="" />
          </button>
        </div>
        <div className="flex gap-[24px] justify-center mt-7">
          {newsData.map((e) => (
            <CardNews
              size="w-[411px]"
              image={e.img}
              title={e.title}
              category={e.category}
              date={e.date}
            />
          ))}
        </div>
      </section>
      {/* End */}
    </div>
  )
}

export default Home
